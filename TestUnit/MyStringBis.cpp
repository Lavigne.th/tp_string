﻿#include "MyStringBis.h"

// constructeur

//PARTIE A
mystringbis::mystringbis()
{
	tete = NULL;
	maj();
}




// PARTIE B
mystringbis::mystringbis(char *pch)
{
	int i;

	for (tete = NULL, i = 0; pch[i] != '\0'; i++)
	{
		insertionfin(pch[i]);
	}
	maj();
}


// on reprend le code de l'exercice 1 cela correspond au constructeur 3
// il n'y es pas dans l'exercice 2
mystringbis::mystringbis(int pn, char c)
{
	this->tete = NULL;
	for (int i = 0; i < pn; i++)
	{
		insertionfin(c);
	}
	maj();
}

// PARTIE D
mystringbis::mystringbis(const mystringbis& s)
{
	this->tete = NULL;
	
	plist nextElement = s.tete;
	while (nextElement != NULL)
	{
		insertionfin(nextElement->data);
		nextElement = nextElement->svt;
	}
	maj();
}

// fonction en plus que j'ai bessoin
// PERMET LA MISE A JOUR CONSTANTE DES VALEUR
void mystringbis::maj()
{
	plist p;
	char c;
	int i;

	spe = 0, length = 0;

	for (int i = 0; i < 26; i++)
		this->stats[i] = 0;

	for (i = 0, p = tete; p != NULL; p = p->svt, i++)  // le p->svt permet d'acc�der � la valeur svt contenue dans la structure qui es abriter par p
	{
		c = tolower(p->data);
		if ((c >= 'a') && (c <= 'z'))
		{
			stats[c - 'a']++;

		}
		else
		{
			spe = spe + 1;
		}
		length = i + 1;
	}
}


void mystringbis::insertionfin(char elt)
{
	plist aux, p;
	aux = (plist)malloc(sizeof(List));
	aux->data = elt;
	aux->svt = NULL;
	length++;

	if (this->tete == NULL)
	{
		this->tete = aux;
	}
	else
	{
		p = tete;
		while (p->svt != NULL)
		{
			p = p->svt;

		}
		p->svt = aux;
	}
}





// destructeur
// PARTIE C
mystringbis::~mystringbis()
{
	// on aurai pas du mettre ceci
	// desalloc(&tete);

	plist p;
	while (tete != NULL)
	{
		p = tete->svt;
		free(tete);
		tete = p;
	}
	cout << "destructeur !!" << endl;
}

//fonction


// PARTIE E
mystringbis& mystringbis::operator=(const mystringbis & s)
{
	plist p;
	if (this != &s)
	{
		this -> ~mystringbis();
		for (p = s.tete, tete = NULL; p != NULL; p = p->svt)
		{
			insertionfin(p->data);
		}
		maj();
	}
	return *this;
}

// PARTIE F
void mystringbis::affiche()
{
	cout << "chaine = " << endl;

	for (plist p = tete; p != NULL; p = p->svt)
	{
		cout << p->data; // endl permet de revenir a la ligne donc on reviens dessus
	}
	cout << endl;
	for (int i = 0; i < 26; i++)
	{
		if (stats[i] != 0)
		{
			cout << (char)(i + 'a') << " = " << stats[i] << endl;
		}
	}
	cout << "nombre de caractère = " << length << endl;
	cout << "nombre de caractère spécial = " << spe << endl;

}

void mystringbis::deleteChar(char c) {
	List* currentElem = this->tete;
	List* previousElement = NULL;
	
	for (int i = 0; currentElem != NULL; i++) {
		if (currentElem->data == c) {
			if(i == 0) {
				this->tete = currentElem->svt;
				free(currentElem);
				currentElem = this->tete;
			}
			else if (currentElem->svt == NULL) {
				free(currentElem);
				currentElem = NULL;
				previousElement->svt = NULL;
			}
			else {
				List* prev = this->tete;
				previousElement->svt = currentElem->svt;
				free(currentElem);
				currentElem = previousElement;
			}
			this->length--;
		}
		previousElement = currentElem;
		if(currentElem != NULL)
			currentElem = currentElem->svt;
	}
}

char mystringbis::getCharAt(int index) {
	List* currentElement = this->tete;
	for (int i = 0; i < index; i++) {
		if (currentElement == NULL)
			return -1;
		currentElement = currentElement->svt;
	}
	return currentElement->data;
}

void mystringbis::toUpperCase() {
	List* currentElement = this->tete;
	while (currentElement != NULL)
	{
		if (currentElement->data >= 97 && currentElement->data <= 122)
			currentElement->data -= 32;
		currentElement = currentElement->svt;
	}
}