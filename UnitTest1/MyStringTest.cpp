#include "CppUnitTest.h"
#include "../TestUnit/MyString.h"
#include "../TestUnit/MyStringBis.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(MyString_Test)
	{
	public:
		
		TEST_METHOD(constructor_empty)
		{
			mystring str;
			Assert::AreEqual(str.getData(), nullptr);
			Assert::AreEqual(str.getLength(), 0);
		}

		TEST_METHOD(constructor_char_array)
		{
			char arr[] = "lol";
			mystring str(arr);
			Assert::AreEqual(str.getData(), "lol");
			Assert::AreEqual(str.getLength(), 3);

			char arr2[] = "j'aime le c++";
			mystring str2(arr2);
			Assert::AreEqual(str2.getData(), "j'aime le c++");
			Assert::AreEqual(str2.getLength(), 13);
		}

		TEST_METHOD(constructor_char)
		{
			mystring str(10, 'c');
			Assert::AreEqual(str.getData(), "cccccccccc");
			Assert::AreEqual(str.getLength(), 10);
		}

		TEST_METHOD(constructor_mystring)
		{
			mystring str(10, 'c');
			mystring str2(str);
			Assert::AreEqual(str2.getData(), str.getData());
			Assert::AreEqual(str2.getLength(), 10);
		}

		TEST_METHOD(get_occurence)
		{
			char arr[] = "des lettres";
			mystring str(arr);
			Assert::AreEqual(str.getCharOccurence('d'), 1);
			Assert::AreEqual(str.getCharOccurence('e'), 3);
			Assert::AreEqual(str.getCharOccurence('s'), 2);
			Assert::AreEqual(str.getCharOccurence('l'), 1);
			Assert::AreEqual(str.getCharOccurence('z'), 0);
		}

		TEST_METHOD(supprime)
		{
			char arr[] = "des lettres";
			mystring str(arr);
			str.supprime('e');
			Assert::AreEqual(str.getData(), "ds lttrs");
			Assert::AreEqual(str.getLength(), 8);

			str.supprime('t');
			Assert::AreEqual(str.getData(), "ds lrs");
			Assert::AreEqual(str.getLength(), 6);

			str.supprime('z');
			Assert::AreEqual(str.getData(), "ds lrs");
			Assert::AreEqual(str.getLength(), 6);
		}

		TEST_METHOD(dedouble)
		{
			char arr[] = "des lettres";
			mystring str(arr);
			str.dedouble('e');
			Assert::AreEqual(str.getData(), "dees leettrees");
			Assert::AreEqual(str.getLength(), 14);

			str.dedouble('d');
			Assert::AreEqual(str.getData(), "ddees leettrees");
			Assert::AreEqual(str.getLength(), 15);

			str.dedouble('z');
			Assert::AreEqual(str.getData(), "ddees leettrees");
			Assert::AreEqual(str.getLength(), 15);
		}

		TEST_METHOD(maj)
		{
			char arr[] = "des lettres3";
			mystring str(arr);
			//Test letter 'e'
			Assert::AreEqual(str.getStats()[4], 3);
			//test letter l
			Assert::AreEqual(str.getStats()[11], 1);
			//test letter z
			Assert::AreEqual(str.getStats()[25], 0);
			//test special char
			Assert::AreEqual(str.getNbSpecialChar(), 2);
			Assert::AreNotEqual(str.getNbSpecialChar(), 3);
		}

		TEST_METHOD(concat)
		{
			char arr[] = "des lettres3";
			mystring str1(arr);
			char arr2[] = "voila ";
			mystring str2(arr2);
			str2.concat(str1);

			Assert::AreEqual(str2.getLength(), 18);
			Assert::AreEqual(str2.getData(), "voila des lettres3");
		}

		TEST_METHOD(minmaj)
		{
			char arr[] = "Des LeTtres1";
			mystring str(arr);
			str.minmaj();

			Assert::AreEqual(str.getLength(), 12);
			Assert::AreEqual(str.getData(), "DES LETTRES1");
		}

		TEST_METHOD(plus_operator)
		{
			char arr1[] = "des ";
			mystring str1(arr1);
			char arr2[] = "lettre";
			mystring str2(arr2);
			mystring str3 = str1 + str2;
			
			Assert::AreEqual(str3.getData(), "des lettre");
			Assert::AreEqual(str3.getLength(), 10);

			Assert::AreEqual(str1.getData(), "des ");
			Assert::AreEqual(str2.getData(), "lettre");
		}

		TEST_METHOD(equal_operator)
		{
			char arr1[] = "des ";
			mystring str1(arr1);
			char arr2[] = "lettre";
			mystring str2(arr2);
			str1 = str2;

			Assert::AreEqual(str1.getData(), "lettre");
			Assert::AreEqual(str1.getLength(), 6);

			Assert::AreEqual(str2.getData(), "lettre");
			Assert::AreEqual(str2.getLength(), 6);
			Assert::AreEqual(str2.getStats()[4], 2);
			Assert::AreEqual(str2.getStats()[4], 2);
			Assert::AreEqual(str2.getStats()[11], 1);
			Assert::AreEqual(str2.getNbSpecialChar(), 0);
		}

		TEST_METHOD(minus_operator)
		{
			char arr1[] = "des lettres";
			mystring str1(arr1);
			mystring str2 = str1 - 'e';

			Assert::AreEqual(str2.getData(), "ds lttrs");
		}

	};

	TEST_CLASS(MyStringBis_Test)
	{
	public:

		TEST_METHOD(constructor_char_array) {
			mystringbis str("chaine");
			Assert::AreEqual(str.getLength(), 6);
			Assert::AreEqual(str.getData()->data, 'c');
			Assert::AreEqual(str.getData()->svt->data, 'h');
			Assert::AreEqual(str.getData()->svt->svt->data, 'a');
		}

		TEST_METHOD(constructor_empty) {
			mystringbis str;
			Assert::AreEqual(str.getLength(), 0);
		}

		TEST_METHOD(constructor_char) {
			mystringbis str(3, 'c');
			Assert::AreEqual(str.getLength(), 3);
			Assert::AreEqual(str.getData()->data, 'c');
			Assert::AreEqual(str.getData()->svt->data, 'c');
			Assert::AreEqual(str.getData()->svt->svt->data, 'c');
		}

		TEST_METHOD(constructor_mystringbis) {
			mystringbis str(3, 'c');
			mystringbis str2(str);
			Assert::AreEqual(str2.getLength(), 3);
			Assert::AreEqual(str2.getData()->data, 'c');
			Assert::AreEqual(str2.getData()->svt->data, 'c');
			Assert::AreEqual(str2.getData()->svt->svt->data, 'c');
		}

		TEST_METHOD(maj) {
			mystringbis str("3 lettres.");
			Assert::AreEqual(str.getStats()[0], 0);
			Assert::AreEqual(str.getStats()[4], 2);
			Assert::AreEqual(str.getStats()[11], 1);
			Assert::AreEqual(str.getLength(), 10);
		}
		
		TEST_METHOD(insertion_fin) {
			mystringbis str("3");
			str.insertionfin('l');
			str.insertionfin('e');
			str.insertionfin('s');
			int len = str.getLength();
			Assert::AreEqual(str.getLength(), 4);
			Assert::AreEqual(str.getData()->data, '3');
			Assert::AreEqual(str.getData()->svt->data, 'l');
			Assert::AreEqual(str.getData()->svt->svt->data, 'e');
			Assert::AreEqual(str.getData()->svt->svt->svt->data, 's');
		}

		TEST_METHOD(equal_ope) {
			mystringbis str1("abc");
			mystringbis str2("cd");

			str1 = str2;
			
			int len = str1.getLength();
			Assert::AreEqual(str1.getLength(), 2);
			Assert::AreEqual(str1.getData()->data, 'c');
			Assert::AreEqual(str1.getData()->svt->data, 'd');
		}

		TEST_METHOD(delete_char) {
			mystringbis str1("abc");

			str1.deleteChar('c');
			Assert::AreEqual(str1.getLength(), 2);
			Assert::AreEqual(str1.getData()->data, 'a');
			Assert::AreEqual(str1.getData()->svt->data, 'b');

			mystringbis str2("abz");

			str2.deleteChar('a');
			Assert::AreEqual(str2.getLength(), 2);
			Assert::AreEqual(str2.getData()->data, 'b');
			Assert::AreEqual(str2.getData()->svt->data, 'z');

			mystringbis str3("abz");

			str3.deleteChar('b');
			Assert::AreEqual(str3.getLength(), 2);
			Assert::AreEqual(str3.getData()->data, 'a');
			Assert::AreEqual(str3.getData()->svt->data, 'z');


			mystringbis str4("des Lettres!");

			str4.deleteChar('e');
			Assert::AreEqual(str4.getLength(), 9);
			Assert::AreEqual(str4.getCharAt(0), 'd');
			Assert::AreEqual(str4.getCharAt(1), 's');
			Assert::AreEqual(str4.getCharAt(2), ' ');
			Assert::AreEqual(str4.getCharAt(3), 'L');
			Assert::AreEqual(str4.getCharAt(4), 't');
			Assert::AreEqual(str4.getCharAt(5), 't');

			str4.deleteChar('s');
			Assert::AreEqual(str4.getLength(), 7);
			Assert::AreEqual(str4.getCharAt(0), 'd');
			Assert::AreEqual(str4.getCharAt(1), ' ');
			Assert::AreEqual(str4.getCharAt(2), 'L');
			Assert::AreEqual(str4.getCharAt(3), 't');
			Assert::AreEqual(str4.getCharAt(4), 't');
			Assert::AreEqual(str4.getCharAt(5), 'r');
			Assert::AreEqual(str4.getCharAt(6), '!');
		}

		TEST_METHOD(get_char_at) {
			mystringbis str("des Lettres!");

			Assert::AreEqual(str.getCharAt(0), 'd');
			Assert::AreEqual(str.getCharAt(1), 'e');
			Assert::AreEqual(str.getCharAt(2), 's');
			Assert::AreEqual(str.getCharAt(3), ' ');
			Assert::AreEqual(str.getCharAt(4), 'L');
			Assert::AreEqual(str.getCharAt(5), 'e');
		}

		TEST_METHOD(to_upper_case) {
			mystringbis str("des Lettres3!");
			str.toUpperCase();

			Assert::AreEqual(str.getCharAt(0), 'D');
			Assert::AreEqual(str.getCharAt(1), 'E');
			Assert::AreEqual(str.getCharAt(2), 'S');
			Assert::AreEqual(str.getCharAt(3), ' ');
			Assert::AreEqual(str.getCharAt(4), 'L');
			Assert::AreEqual(str.getCharAt(5), 'E');
			Assert::AreEqual(str.getCharAt(6), 'T');
			Assert::AreEqual(str.getCharAt(7), 'T');
			Assert::AreEqual(str.getCharAt(8), 'R');
			Assert::AreEqual(str.getCharAt(9), 'E');
			Assert::AreEqual(str.getCharAt(10), 'S');
			Assert::AreEqual(str.getCharAt(11), '3');
			Assert::AreEqual(str.getCharAt(12), '!');
		}
	};
}