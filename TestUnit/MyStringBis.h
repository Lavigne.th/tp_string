﻿#pragma once
#include <iostream>
#include <cstdio>
#include <cstddef>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

typedef struct liste
{
	char data;
	struct liste *svt;

}List, *plist; // on doit mettree un L majuscule a List car il existe deja une biblioth�que qui comporte
// list ( c'est pour eviter les conflit de nom

class mystringbis
{
private:
	plist tete;
	int length, spe;
	int stats[26];
	void maj();
public:
	mystringbis();
	mystringbis(char *);
	mystringbis(int, char);
	mystringbis(const mystringbis &);
	~mystringbis();
	mystringbis & operator = (const mystringbis &);;
	void affiche();
	void deleteChar(char c);

	void insertionfin(char elt);
	char getCharAt(int index);
	void toUpperCase();

	int getLength() { return length; }
	plist getData() { return tete; }
	int* getStats() { return stats; }
	int getNbSpecialChar() { return spe; }
};

