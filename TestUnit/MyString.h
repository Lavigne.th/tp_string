#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>


using namespace std;

class mystring
{
private:
	char *tab;
	int nbLetter;
	int stat[26];
	int spe;

public:

	mystring();
	mystring(char *);
	mystring(int, char);
	~mystring();
	mystring(const mystring &);
	void affiche();
	void mise_jour_stat();


	void supprime(char);
	void dedouble(char);
	void concat(const mystring &);
	void minmaj();

	int getCharOccurence(const char c);

	const char* getData() { return tab; }
	int getLength() { return nbLetter; }
	const int* getStats() { return stat; }
	int getNbSpecialChar() { return spe; }

	//Exercice 2
	// operateur

	mystring operator+(const mystring&);
	mystring& operator=(const mystring&);
	mystring operator-(char);
	friend ostream& operator<< (ostream&, const mystring&);//ostream correspond au flux de sortie
};