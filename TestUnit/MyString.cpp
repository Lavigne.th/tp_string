#include "MyString.h"

// constructeur


//partie A

mystring::mystring()
{
	nbLetter = 0;
	spe = 0;
	this->tab = nullptr;
	for (int i = 0; i < 26; i++)
	{
		stat[i] = 0;
	}
}


//------------------------------------------------------
// partie B
mystring::mystring(char *source)
{
	tab = new char[strlen(source) + 1];
	strcpy(tab, source); // fonction copie de string.h
	mise_jour_stat();
}


//---------------------------------------------------------
// Parti C
mystring::mystring(int  nb_cara, char cara)
{

	int i;
	tab = new char[(nb_cara + 1)];

	for (i = 0; i < nb_cara; i++)
	{
		tab[i] = cara;
	}
	tab[i] = '\0';
	mise_jour_stat();
}


//---------------------------------------------------
//Partie E
mystring::mystring(const mystring& source)
{
	tab = new char[strlen(source.tab) + 1];
	strcpy(tab, source.tab);
	mise_jour_stat();
}


//-----------------------------------------------------
// destructeur
// Partie D
mystring::~mystring()
{
	cout << "destructeur" << endl;
	delete tab;
}


// ---------------------------------------------------------
// PARTIE f
// ACCESSEUR




// ---------------------------------------------------------
// PARTIE G
void mystring::affiche()
{
	int compteur;

	cout << "Chaine =" << tab << endl;
	cout << "Votre chaine contient :" << nbLetter << "caractere(s)" << endl;
	cout << "Votre chaine contient :" << spe << " caracteres speciaux" << endl;
	for (compteur = 0; compteur < 26; compteur++)
	{
		if (stat[compteur] != 0)
		{
			cout << "Il y a " << (stat[compteur]) << "  caractere(s) de type  " << (char)('a' + compteur) << endl;
		}
	}
}


int mystring::getCharOccurence(const char c) {
	int occurence = 0;
	for (int compteur = 0; tab[compteur] != '\0'; compteur++)
		if (tab[compteur] == c)
			occurence++;
	return occurence;
}


//-----------------------------------------------------------
// PARTIE H
void mystring::supprime(char cara)
{
	int j, compteur2;

	int compteur = getCharOccurence(cara);

	if (compteur != 0)
	{
		char* transition = new char[nbLetter - compteur + 1];

		for (compteur2 = 0, j = 0; tab[compteur2] != '\0'; compteur2++)
		{
			if (tab[compteur2] != cara)
			{
				transition[j] = tab[compteur2];
				j++;
			}
		}
		transition[j] = '\0';
		delete tab;
		tab = transition;
		mise_jour_stat();
	}
}

//------------------------------------------------------------
// PARTIE I
void mystring::dedouble(char cara)
{
	int j = 0, compteur2;

	int compteur = getCharOccurence(cara);

	if (compteur != 0)
	{
		char* transition = new char[nbLetter + compteur + 1];

		for (compteur2 = 0, j = 0; tab[compteur2] != '\0'; compteur2++)
		{
			transition[j++] = tab[compteur2];

			if (tab[compteur2] == cara)
			{
				transition[j++] = tab[compteur2];

			}
		}
		transition[j] = '\0';

		delete tab;
		tab = transition;
	}
	mise_jour_stat();
}

// fonction mise � jour
void mystring::mise_jour_stat()
{
	int compteur, indice;
	char car;
	spe = 0, nbLetter = 0;

	for (int i = 0; i < 26; i++)
	{
		stat[i] = 0;
	}


	for (compteur = 0; tab[compteur] != '\0'; compteur++)
	{
		car = tolower(tab[compteur]);

		if ((car >= 'a') && (car <= 'z'))
		{
			indice = car - 'a';
			stat[indice]++;

		}
		else
		{
			spe++;
		}
		nbLetter++;
	}
}




void mystring::concat(const mystring & source)
{
	int compteur, compteur2, taille;

	taille = nbLetter + source.nbLetter + 1;
	//char transition[taille];
	char *transition = new char[taille];

	for (compteur = 0; tab[compteur] != '\0'; compteur++)
	{
		transition[compteur] = tab[compteur];
	}

	for (compteur2 = 0; source.tab[compteur2] != '\0'; compteur2++)
	{
		transition[compteur++] = source.tab[compteur2];
	}

	transition[compteur] = '\0';

	delete tab;
	tab = transition;
	mise_jour_stat();

}



//-------------------------------------------------------------
// PARTIE K
void mystring::minmaj()
{
	int i = 0;

	for (i = 0; tab[i] != '\0'; i++)
	{
		tab[i] = toupper(tab[i]);
	}
}


//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------
//              EXERCICE 2




mystring& mystring::operator=(const mystring& md)
{
	if (this != &md)
	{
		delete tab;

		nbLetter = md.nbLetter;
		spe = md.spe;
		tab = new char[nbLetter + 1];
		strcpy(tab, md.tab);
		for (int i = 0; i < 26; i++)
		{
			stat[i] = md.stat[i];
		}
	}
	return (*this);
}


mystring mystring::operator+(const mystring& s2)
{
	mystring sbis(*this);
	sbis.concat(s2);
	return sbis;
}

mystring mystring::operator-(char car)
{
	mystring sbis(*this); // etoile de this car on es dans l'instance !!
	sbis.supprime(car);
	mise_jour_stat();
	return sbis;
}


ostream& operator<<(ostream& os, const mystring& obj)
{
	os << "chaine = " << obj.tab << endl;
	os << "n = " << obj.nbLetter << endl;
	os << "spe = " << obj.spe << endl;

	for (int i = 0; i < 26; i++)
	{
		if (obj.stat[i] != 0)
		{
			os << obj.stat[i] << " car " << (char)(i + 'a') << endl;
		}
	}
	return os;
}


